package v4;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class CreateInstance {

	public static void main(String[] args) throws Exception {
//		Person person = new Person(1, "name1");
		
		// New instance using reflection
		Constructor<Person> constructor = Person.class.getConstructor(int.class, String.class);
		Person person = constructor.newInstance(1, "name1");
		System.out.println(person);
		
		// invoke method using reflection
		Method method = Person.class.getMethod("sayHello", String.class);
		method.invoke(person, "Hello");
		
		// set param using reflection
		Field field = person.getClass().getDeclaredField("id");
		field.setAccessible(true);
		field.set(person, 10);
		System.out.println(person);
	}

}
