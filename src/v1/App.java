package v1;

public class App {
	public static void main(String[] args) {
		Person p = new Person();
		p.setI(5);
		
		Class<? extends Person> clazz = p.getClass();
		System.out.println(clazz.getName());
		
		System.out.println(p.getClass());
		
		Employee e = new Employee();
		System.out.println(e.getClass());
		
		p = new Employee();
		
		System.out.println(p.getClass());
	}
}