package v2;

public class WaystoGet {

	public static void main(String[] args) {
		// 1
		Person p = new Person();
		System.out.println(p.getClass());

		// 2
		try {
			Class<?> p1 = Class.forName("v2.Person");
			System.out.println(p1);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		// 3   T - type    T.class
		
		System.out.println(Person.class);
		System.out.println(int.class);
		System.out.println(int[].class.getCanonicalName());
		System.out.println(Double[].class.getSimpleName());
		
		// p.getClass() == Person.class == Class.forName("v2.Person")
		
		p = new Person();
		Employee e = new Employee();
				
		
		foo(p);
		foo(e);
		
	}
	
	static void foo(Object obj) {
		System.out.println(obj.getClass() + " instanceof Person: " + (obj instanceof Person));
		
		System.out.println(obj.getClass() + " obj.getClass() == Person.class: " + (obj.getClass() == Person.class));
	}
}












