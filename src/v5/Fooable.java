package v5;

public interface Fooable {
	void sayHello(String str);
	void sayBay(String str, int num);
}
