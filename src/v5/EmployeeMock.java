package v5;

public class EmployeeMock extends Employee {
	
	@Override
	public void sayHello(String str) {
		long start = System.nanoTime();
		super.sayHello(str);
		long end = System.nanoTime();
		
		System.out.println(end - start);
	}

	@Override
	public void sayBay(String str, int num) {
		long start = System.nanoTime();
		super.sayBay(str, num);
		long end = System.nanoTime();
		
		System.out.println(end - start);
	}
}
