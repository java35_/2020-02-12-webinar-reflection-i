package v5;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class App {
	public static void main(String[] args) {
		Fooable f = new Employee();
		InvocationHandler handler = new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) 
						throws Throwable {
				long start = System.nanoTime();
				method.invoke(f, args);
				long end = System.nanoTime();
				System.out.println(end - start);
				return null;
			}
		};
		
		Fooable foo = (Fooable)Proxy.newProxyInstance(
						Employee.class.getClassLoader(), 
						Employee.class.getInterfaces(), 
						handler);
		foo.sayHello("Hello");
		foo.sayBay("Bay", 10);
		
//		EmployeeMock employeeMock = new EmployeeMock();
//		employeeMock.sayHello("Hello");
//		employeeMock.sayBay("Bay", 10);
	}
}