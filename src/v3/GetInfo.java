package v3;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.List;

public class GetInfo {
	public static void main(String[] args) throws Exception {
		// Field
		// Method
		// Constructor
		// Parameter
		// Modifier
		
		Class<Employee> clazz = Employee.class;
		
		System.out.println("Class: ");
		System.out.println(Modifier.toString(clazz.getModifiers())
				+ " " + clazz);
		System.out.println(clazz.getName());
		System.out.println(clazz.getPackageName());
		System.out.println(clazz.getCanonicalName());
		System.out.println(clazz.getSimpleName());
		System.out.println(clazz.getTypeName());
		
		System.out.println();
		System.out.println("Fields:");
		
//		Field[] fields = clazz.getFields(); // only public fields
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			System.out.println(field + " : " + 
				Modifier.toString(field.getModifiers()) + " " +
					field.getType().getSimpleName() + " " + 
					field.getName());
		}
		
		System.out.println();
		System.out.println("Methods:");
//		Method[] methods = clazz.getMethods();
		Method[] methods = clazz.getDeclaredMethods();
		for (Method method : methods) {
			System.out.print(method + " : ");
			System.out.println(Modifier.toString(method.getModifiers()) + " " + 
					method.getReturnType().getSimpleName() + " " +
					method.getName());
			Parameter[] parameters = method.getParameters();
			for (Parameter parameter : parameters) {
				System.out.println(parameter.getType().getSimpleName() + " " + parameter.getName());
			}
		}
		
		System.out.println();
		System.out.println("Constructors:");
//		Constructor<?>[] constructors = clazz.getConstructors();
		Constructor<?>[] constructors = clazz.getDeclaredConstructors();
		for (Constructor<?> constructor : constructors) {
			System.out.print(constructor + " : ");
			System.out.println(Modifier.toString(constructor.getModifiers()) + " " +
					constructor.getName());
			Parameter[] parameters = constructor.getParameters();
			for (Parameter parameter : parameters) {
				System.out.println(parameter.getType().getSimpleName() + " " + parameter.getName());
			}
		}
	}
}









