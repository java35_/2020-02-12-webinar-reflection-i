package v3;

import java.util.ArrayList;
import java.util.List;

public class Employee {
	public int i1;
	int i2;
	protected int i3[];
	private int i4;
	
	public static String str1;
	private final String str2 = "123";
	private static final List<String> array = new ArrayList();
	
	private Employee(int i1) {
		super();
		this.i1 = i1;
	}

	public Employee() {
	}

	public Employee(int i1, int i2, int[] i3, int i4) {
		this.i1 = i1;
		this.i2 = i2;
		this.i3 = i3;
		this.i4 = i4;
	}

	public void foo1() {
		
	}
	
	private void foo2(String str, int j) {
		
	}
	
	String foo3() {
		return "";
	}
	
	static public void foo4() {
		
	}
	
	@Override
	public String toString() {
		return "";
	}
}
